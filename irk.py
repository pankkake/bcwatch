#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

import json
import os
import socket
import sys
from decimal import Decimal

from bcwatch import Watcher

TARGET = os.getenv('IRK_TARGET', 'irc://chat.freenode.net/paymium')


class MyWatcher(Watcher):
    SLEEP = 30

    def log(self, error):
        print(error, file=sys.stderr)


class Irk(object):
    MINIMUM = Decimal(0.1)

    def __init__(self):
        self.last = None

    def __call__(self, trade):
        ticker = trade.get('ticker') or {}
        low = ticker.get('low')
        high = ticker.get('high')
        if high and trade['price'] >= high:
            hl = '24H HIGH'
        elif low and trade['price'] <= low:
            hl = '24H LOW'
        else:
            hl = ''

        # ignore very small trades
        if trade['traded_btc'] < self.MINIMUM:
            return

        if self.last is None:
            direction = ' '
        elif self.last > trade['price']:
            direction = '▼'
        elif self.last < trade['price']:
            direction = '▲'
        else:
            direction = ' '
            hl = ''
        self.last = trade['price']

        trade['hl'] = hl
        trade['direction'] = direction

        message = '{traded_btc:7.3f} BTC ' \
            '@ {price:.2f} {currency} {direction} {hl:>9}' \
            .format(**trade).rstrip().replace(' 0.', '  .', 1)

        data = {'to': TARGET, 'privmsg': message}
        s = socket.create_connection(('localhost', 6659))
        s.sendall(json.dumps(data))

w = MyWatcher()
irk = Irk()
w.watch(irk, ticker=True)
