#!/usr/bin/env python
from __future__ import absolute_import, print_function, unicode_literals

import time
from decimal import Decimal

import requests


class Watcher(object):
    URL = 'https://paymium.com/api/v1/data/eur/trades'
    TICKER_URL = 'https://paymium.com/api/v1/data/eur/ticker'
    SLEEP = 60

    def __init__(self, session=None, uuid=None, etag=None):
        self.uuid = uuid
        self.etag = etag
        if not session:
            session = requests.Session()
            session.timeout = 120
            session.verify = True
        self.session = session
        self.ticker = None

    def sleep(self):
        time.sleep(self.SLEEP)

    def watch(self, callback, assume_new=False, ticker=False):
        while True:
            self.update(callback, assume_new, ticker)
            self.sleep()

    def update(self, callback, assume_new=False, ticker=False):
        if self.should_fetch():
            if ticker:
                self.update_ticker()
            for newtrade in self.fetch(seen=not assume_new):
                if ticker:
                    newtrade['ticker'] = self.ticker
                callback(newtrade)

    def should_fetch(self):
        try:
            r = self.session.head(self.URL)
            r.raise_for_status()
        except requests.exceptions.RequestException as error:
            self.log(error)
            return False
        etag = r.headers.get('ETag')
        if etag is None or self.etag != etag:
            self.etag = etag
            return True
        return False

    def log(self, error):
        pass

    def fetch(self, seen=True):
        try:
            r = self.session.get(self.URL, stream=False)
            r.raise_for_status()
            trades = r.json(parse_float=Decimal)
        except (ValueError, requests.exceptions.RequestException) as error:
            self.log(error)
            return
        # if has been run once already; disable assume_new
        if self.uuid:
            seen = True
        for trade in trades:
            uuid = trade['uuid']
            if not seen:
                yield trade
            elif uuid == self.uuid:
                # everything after last known uuid is unseen
                seen = False
        # if it happens, it means we missed trades, and that case
        # isn't handled yet
        assert seen is False or self.uuid is None
        self.uuid = uuid

    def update_ticker(self):
        try:
            r = self.session.get(self.TICKER_URL, stream=False)
            r.raise_for_status()
            ticker = r.json(parse_float=Decimal)
            self.ticker = ticker
        except (ValueError, requests.exceptions.RequestException) as error:
            self.log(error)
        return self.ticker


if __name__ == '__main__':
    w = Watcher()
    w.watch(print, True)
