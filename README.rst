Paymium watcher
===============

**bcwatch** is a tool and library able to stream new
`Paymium.com <https://paymium.com/>`_ trades.
It only fetches the trades feed when new trades are available, making
it light in bandwidth and processing power.
It will also use the proper types for currencies (``Decimal`` over ``float``).


Requirements
------------

- Python 2.6 or higher
- requests 1.2 or higher


Basic usage
-----------

By default running ``./bcwatch.py`` will stream JSON entries
to the standard input as they appear; one per line.


Advanced usage
--------------

The ``bcwatch.Watcher`` object accepts any callback for new trades.
Example::

    from __future__ import print_function, unicode_literals
    from bcwatch import Watcher

    def handle_trade(trade):
        print('%s BTC @ %s %s'
              % (trade['traded_btc'], trade['price'], trade['currency']))

    w = Watcher()
    w.watch(handle_trade)


If you don't want to run the loop, you can use ``run()``
instead of ``watch()``, and even save the ``etag`` and ``uuid``
to restore the previous ``Watcher`` state,
if you destroy the class (e.g. by closing the application).

If you want all trades to be considered new
on the first run, you should call it with ``assume_new=True``.


For more customization, the ``bcwatch.Watcher`` class is made to be extended.
Network/API errors are ignored by default, but you can override
the ``log()`` method.


Credits
-------

If you like this script, please tip 1Md5kvYempaW3NkYxdYbFezL6mKg7QwERe.
